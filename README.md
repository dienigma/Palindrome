This is a try out for setting up test driven development.
The goal of this repo is to understand and implement automated test driven development.
I will be using Mocha for this mostly.

The module can be used as follows:
```
$ npm install --global cj-palindrome
$ vim test.js
let Phrase = require("cj-palindrome");
let napoleonsLament = new Phrase("Able was I, ere I saw Elba.");
console.log(napoleonsLament.palindrome());
$ node test.js
true
```
